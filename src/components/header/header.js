import React from 'react';
import logo from '../assets/logo.png'
import './header.css'

const Header = () => {
    return (
        <div className='header'>
            <img className="logo" src={logo} alt="logo" />
            <h4>MilanTV</h4>
            <input className="input-search" type="text" placeholder="Search Movie" />
            <button className="signin-btn">Sign In</button>
        </div>
    );
};

export default Header;