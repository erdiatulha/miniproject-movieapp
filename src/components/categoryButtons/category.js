import React from 'react';
import './category.css'

const Category = () => {
    return (
        <div className="btn-wrapper">
            <h4>Browse by category</h4>
            <div className="category-btn">
                <button>All</button>
                <button>Anime</button>
                <button>Action</button>
                <button>Adventure</button>
                <button>Science Fiction</button>
                <button>Comedy</button>
            </div>
        </div>
    );
};

export default Category;