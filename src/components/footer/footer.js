import React from 'react';
import './footer.css'
import logo from '../assets/logo.png'
import google from '../assets/googlestore.png'
import apple from '../assets/applestore.png'
import facebook from '../assets/fb.svg'
import pinterest from '../assets/pinterest.png'
import instagram from '../assets/ig.png'


const Footer = () => {
    return (
        <React.Fragment>
            <div className="footer">
                <div className="footer-row">
                    <div className="footer-brand">
                        <div className="footer-brand-logo">
                            <img className="logo-image" src={logo} alt="logo" />
                            <h4>MilanTV</h4>
                        </div>
                    <p id="lorem">
                        Lorem Ipsum is simply dummy text of the printing and typesetting
                        industry. Lorem Ipsum has been the industry's standard.printing and
                        typesetting industry. Lorem Ipsum has been the industry's standard
                    </p>
                    </div>
                    <div className="footer-link">
                        <p>Tentang Kami</p>
                        <p>Blog</p>
                        <p>Layanan</p>
                        <p>Karir</p>
                        <p>Pusat Media</p>
                    </div>
                    <div className="footer-media">
                        <h5>Download</h5>
                        <img
                            className="google"
                            src={google}
                            alt="Play Store"
                        />
                        <img 
                            className="apple"
                            src={apple}
                            alt="Apple Store"
                        />
                    <div className="soc-med">
                        <h5>Social Media</h5>
                        <img
                            className="fb"
                            src={facebook}
                            alt="Facebook"
                            width="5%"
                            height="5%"
                        />
                        <img
                            className="pin"
                            src={pinterest}
                            alt="Pinterest"
                            width="5%"
                            height="5%"
                        />
                        <img
                            className="ig"
                            src={instagram}
                            alt="Instagram"
                            width="5%"
                            height="5%"
                        />
                    </div>
                </div>
            </div>
            <hr />
            <div className="copyright">
                <p>Copyright © 2000-202 MilanTV. All Rights Reserved</p>
            </div>
            </div>
        </React.Fragment>
    );
};

export default Footer;