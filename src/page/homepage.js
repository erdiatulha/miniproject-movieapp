import React from 'react';
import Category from '../components/categoryButtons/category';
import Footer from '../components/footer/footer';
import Header from '../components/header/header'
import Slider from '../components/slider/slider'

const Homepage = () => {
    return (
        <div>
            <Header />
            <Slider />
            <Category />
            <Footer />
        </div>
    );
};

export default Homepage;