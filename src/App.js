import './App.css';
import Homepage from './page/homepage'

function App() {
  return (
    <div>
      <Homepage />
    </div>
  );
}

export default App;
